import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Web 0.2
import com.canonical.Oxide 1.0 as Oxide

import "components"
import "BrowserDialogs" as BrowserDialogs

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'turtl-ut.cibersheep'

    automaticOrientation: true
    anchorToKeyboard: true

    anchors.fill: parent

	property bool nightMode: false
	property string lighterColor: "#333333"
	property string darkColor: "#efefef"

    Page {
        id: page
        anchors {
            fill: parent
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height

        header: AppHeader {
            id: header
            visible: false
        }

        WebContext {
            id: webcontext
            userAgent: 'Mozilla/5.0 (Linux; Android 5.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.102 Mobile Safari/537.36 Ubuntu Touch Webapp'
            userScripts: [
                Oxide.UserScript {
                    context: 'oxide://main-world'
                    emulateGreasemonkey: true
                    url: Qt.resolvedUrl('injectcss.js')
                },
                Oxide.UserScript {
                        context: "messaging://"
                        url: Qt.resolvedUrl("message.js")
                    }
            ]
        }

        WebView {
            id: webview
            anchors {
                top: header.visible ? header.bottom : parent.top
                bottom: parent.bottom
            }
            width: parent.width
            height: parent.height

            alertDialog: BrowserDialogs.AlertDialog {}
            confirmDialog: BrowserDialogs.ConfirmDialog {}
            promptDialog: BrowserDialogs.PromptDialog {}
            //beforeUnloadDialog: BrowserDialogs.BeforeUnloadDialog {}
            filePicker: pickerComponent

            context: webcontext
            url: Qt.resolvedUrl('www/index.html')
            preferences.localStorageEnabled: true
            preferences.appCacheEnabled: true
            
            preferences.allowUniversalAccessFromFileUrls: true

            Component.onCompleted: {
                preferences.localStorageEnabled = true;
            }
        }

        ProgressBar {
            height: units.dp(3)
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            showProgressPercentage: false
            value: (webview.loadProgress / 100)
            visible: (webview.loading && !webview.lastLoadStopped)
        }

        Component {
            id: pickerComponent

            BrowserDialogs.PickerDialog {}
        }

        function executeJavaScript(code) {
			var req = webview.rootFrame.sendMessage("messaging://", "EXECUTE", {code: code});
			req.onerror = function (code, explanation) {
				console.log("Error " + code + ": " + explanation)
			}
		}
    }
    

}
