function handler(event) {
    console.log('Injecting ubuntu touch styling fixes');

    var style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(
        '.notes-edit form textarea, form.standard-form { padding-top: 0; } ' +
        '.noct { background-color: #333333 !important; color: #f6f6f5 } ' 
    ));

    document.head.appendChild(style);
}

window.addEventListener('load', handler, false);
document.body.setAttribute("id", "body");
